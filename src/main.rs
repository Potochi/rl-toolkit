#![feature(intrinsics, core_intrinsics)]

use std::fmt::Debug;
use std::intrinsics;
use std::io::Read;
use std::net::Ipv4Addr;
use std::path::Path;
use std::str::FromStr;
use std::{fs, io};

fn required_bits(x: u32) -> u8 {
    32 - intrinsics::ctlz(x) as u8
}

fn round_next_power_of_two(x: u32) -> u32 {
    1 << required_bits(x)
}

#[derive(Clone, Default)]
struct SubnetReqirment {
    name: String,
    assignable: u32,
}

#[derive(Debug)]
struct Subnet {
    name: String,
    size: u32,
    required: u32,
    network: Ipv4Addr,
    broadcast: Ipv4Addr,
    mask: Ipv4Addr,
}

fn read_subnetting_requirments<P: AsRef<Path>>(path: P) -> io::Result<Vec<SubnetReqirment>> {
    let mut reader = io::BufReader::new(fs::File::open(path)?);

    let mut contents = String::new();
    reader.read_to_string(&mut contents)?;

    let reqs: Vec<SubnetReqirment> = contents
        .trim()
        .split("\n")
        .map(|line| {
            let (name, required) = line.trim().split_once("=").expect("Invalind input format");

            SubnetReqirment {
                name: name.to_owned(),
                assignable: u32::from_str(required).expect("Invalid input format"),
            }
        })
        .collect::<Vec<SubnetReqirment>>();

    Ok(reqs)
}

fn subnet_network(addr: Ipv4Addr, _mask: u8, requirments: Vec<SubnetReqirment>) -> Vec<Subnet> {
    let mut subnets = Vec::<Subnet>::new();
    let mut reqs = requirments.clone();
    reqs.sort_by(|a, b| b.assignable.cmp(&a.assignable));

    let mut iaddr = u32::from_be_bytes(addr.octets());

    for req in reqs {
        let subnet_size = round_next_power_of_two(req.assignable);
        let host_bits = required_bits(req.assignable);

        subnets.push(Subnet {
            name: req.name.to_owned(),
            size: subnet_size,
            required: req.assignable,
            network: Ipv4Addr::from(iaddr),
            broadcast: Ipv4Addr::from(iaddr + subnet_size - 1),
            mask: Ipv4Addr::from(((0 as u32).wrapping_sub(1) >> host_bits) << host_bits),
        });

        iaddr += subnet_size;
    }

    subnets
}

fn main() -> io::Result<()> {
    let args = std::env::args().collect::<Vec<String>>();

    if args.len() != 4 {
        eprintln!("./rltoolkit <address> <mask> <requirments-path>");
        return Ok(());
    }

    let reqs = read_subnetting_requirments(&args[3])?;

    println!(
        "{:#?}",
        subnet_network(
            Ipv4Addr::from_str(&args[1]).expect("Failed to parse addr"),
            16,
            reqs
        )
    );

    Ok(())
}
